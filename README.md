# Frame Recorder

This project aims to provide a format and an API for recording and replaying data per frame in an application.
That data can for example be object location data in a 3D game or similar.

The format is built around the concepts of a runtime-specific number of objects, that provides a compile-time-specific number of properties that should be recorded/replayed.

In the repo currently exist an API for C# (Unity3D) and C++, as well as a working example for Unity (LocationRecorder/LocationReplayer).


## C++ API

### Recording

    framerecorder::writer::RecordWriter<float> writer(objectNames, frameRate);
    std::vector<std::tuple<float>> values(objects.size());
    
    while (running)
    {
        int i = 0;
        for (object & o : objects)
            values[i++] = o.getData();
        writer.setValues(values);
    }
    
    writer.save(fileName);

### Replaying

    framerecorder::reader::RecordReader<float> reader;
    reader.load(fileName);
    std::vector<std::tuple<float>> values(reader.getObjects().size());
    
    while (reader.getValues(values))
    {
        int i = 0;
        for (object & o : objects)
            o.setData(values[i++]);
    }