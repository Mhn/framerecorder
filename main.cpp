
#include "RecordReader.hpp"
#include "RecordWriter.hpp"

#include <cmath>
#include <iostream>

void testSinus1()
{
  std::vector<uint32_t> loops({ 1, 10, 100, 1000, 10000, 100000, 1000000 });
  std::stringstream log;

  for (uint32_t l : loops)
  {
    {
      std::vector<std::string> objects({ "o" });

      framerecorder::writer::RecordWriter<float> writer(objects, 73);

      std::vector<std::tuple<float>> stream(objects.size());

      for (uint32_t i = 0; i < l; i++)
      {
        float v = std::sin(i + 1);

        if (writer.getNumberOfFrames() != i)
          std::cerr << "writer.numberOfFrames mismatch: " << writer.getNumberOfFrames() << " != " << i << std::endl;

        std::get<0>(stream[0]) = v;
        if (!writer.setValues(stream, log))
          std::cerr << "setValues error: " << log.str() << std::endl;
      }

      if (writer.getNumberOfFrames() != l)
        std::cerr << "writer.numberOfFrames mismatch: " << writer.getNumberOfFrames() << " != " << l << std::endl;

      if (!writer.save("test.bin"))
        std::cerr << "save error" << std::endl;
    }
    {
      framerecorder::reader::RecordReader<float> reader;

      if (!reader.load("test.bin", log))
        std::cerr << "load error: " << log.str() << std::endl;

      if (reader.getNumberOfFrames() != l)
        std::cerr << "reader.numberOfFrames mismatch: " << reader.getNumberOfFrames() << " != " << l << std::endl;

      if (reader.getFrameRate() != 73)
        std::cerr << "reader.frameRate mismatch: " << reader.getFrameRate() << " != " << 73 << std::endl;

      std::vector<std::string> objects = reader.getObjects();

      if (objects.size() != 1)
        std::cerr << "objects.size mismatch: " << objects.size() << " != " << 1 << std::endl;

      if (reader.getCurrentFrame() != 0)
        std::cerr << "reader.currentFrame pre mismatch: " << reader.getCurrentFrame() << " != " << 0 << std::endl;

      std::vector<std::tuple<float>> stream(objects.size());

      for (uint32_t i = 0; i < reader.getNumberOfFrames(); i++)
      {
        float v = std::sin(i + 1);
        if (!reader.getValues(stream, log))
          std::cerr << "getValues error: " << log.str() << std::endl;

        if (reader.getCurrentFrame() != i)
          std::cerr << "reader.currentFrame mismatch: " << reader.getCurrentFrame() << " != " << i << std::endl;

        if (std::get<0>(stream[0]) != v)
          std::cerr << "value mismatch: " << std::get<0>(stream[0]) << " != " << v << std::endl;

        if (i + 1 < reader.getNumberOfFrames())
          if (!reader.advanceFrames())
            std::cerr << "advanceFrames error" << std::endl;
      }
    }
  }
}

void testSinus1b()
{
  std::vector<uint32_t> loops({ 1, 10, 100, 1000, 10000, 100000, 1000000 });
  std::stringstream log;

  for (uint32_t l : loops)
  {
    {
      std::vector<std::string> objects({ "o" });

      framerecorder::writer::RecordWriter<float> writer(objects, 73);

      std::vector<std::tuple<float>> stream(objects.size());

      for (uint32_t i = 0; i < l; i++)
      {
        float v = std::sin(std::floor(i / 10));

        if (writer.getNumberOfFrames() != i)
          std::cerr << "writer.numberOfFrames mismatch: " << writer.getNumberOfFrames() << " != " << i << std::endl;

        std::get<0>(stream[0]) = v;
        if (!writer.setValues(stream, log))
          std::cerr << "setValues error: " << log.str() << std::endl;
      }

      if (writer.getNumberOfFrames() != l)
        std::cerr << "writer.numberOfFrames mismatch: " << writer.getNumberOfFrames() << " != " << l << std::endl;

      if (!writer.save("test.bin"))
        std::cerr << "save error" << std::endl;
    }
    {
      framerecorder::reader::RecordReader<float> reader;

      if (!reader.load("test.bin", log))
        std::cerr << "load error: " << log.str() << std::endl;

      if (reader.getNumberOfFrames() != l)
        std::cerr << "reader.numberOfFrames mismatch: " << reader.getNumberOfFrames() << " != " << l << std::endl;

      if (reader.getFrameRate() != 73)
        std::cerr << "reader.frameRate mismatch: " << reader.getFrameRate() << " != " << 73 << std::endl;

      std::vector<std::string> objects = reader.getObjects();

      if (objects.size() != 1)
        std::cerr << "objects.size mismatch: " << objects.size() << " != " << 1 << std::endl;

      if (reader.getCurrentFrame() != 0)
        std::cerr << "reader.currentFrame pre mismatch: " << reader.getCurrentFrame() << " != " << 0 << std::endl;

      std::vector<std::tuple<float>> stream(objects.size());

      for (uint32_t i = 0; i < reader.getNumberOfFrames(); i++)
      {
        float v = std::sin(std::floor(i / 10));

        if (!reader.getValues(stream, log))
          std::cerr << "getValues error: " << log.str() << std::endl;

        if (reader.getCurrentFrame() != i)
          std::cerr << "reader.currentFrame mismatch: " << reader.getCurrentFrame() << " != " << i << std::endl;

        if (std::get<0>(stream[0]) != v)
          std::cerr << "value mismatch: " << std::get<0>(stream[0]) << " != " << v << std::endl;

        if (i + 1 < reader.getNumberOfFrames())
          if (!reader.advanceFrames())
            std::cerr << "advanceFrames error" << std::endl;
      }
    }
  }
}

void testSinus2()
{
  std::vector<uint32_t> loops({ 1, 10, 100, 1000, 10000, 100000, 1000000 });
  std::stringstream log;

  for (uint32_t l : loops)
  {
    {
      std::vector<std::string> objects({ "o", "p" });

      framerecorder::writer::RecordWriter<float> writer(objects, 73);

      std::vector<std::tuple<float>> stream(objects.size());

      for (uint32_t i = 0; i < l; i++)
      {
        float v1 = std::sin(i + 1);
        float v2 = std::sin(i - 1);

        if (writer.getNumberOfFrames() != i)
          std::cerr << "writer.numberOfFrames mismatch: " << writer.getNumberOfFrames() << " != " << i << std::endl;

        std::get<0>(stream[0]) = v1;
        std::get<0>(stream[1]) = v2;
        if (!writer.setValues(stream, log))
          std::cerr << "setValues error: " << log.str() << std::endl;
      }

      if (writer.getNumberOfFrames() != l)
        std::cerr << "writer.numberOfFrames mismatch: " << writer.getNumberOfFrames() << " != " << l << std::endl;

      if (!writer.save("test.bin"))
        std::cerr << "save error" << std::endl;
    }
    {
      framerecorder::reader::RecordReader<float> reader;

      if (!reader.load("test.bin", log))
        std::cerr << "load error: " << log.str() << std::endl;

      if (reader.getNumberOfFrames() != l)
        std::cerr << "reader.numberOfFrames mismatch: " << reader.getNumberOfFrames() << " != " << l << std::endl;

      if (reader.getFrameRate() != 73)
        std::cerr << "reader.frameRate mismatch: " << reader.getFrameRate() << " != " << 73 << std::endl;

      std::vector<std::string> objects = reader.getObjects();

      if (objects.size() != 2)
        std::cerr << "objects.size mismatch: " << objects.size() << " != " << 2 << std::endl;

      if (reader.getCurrentFrame() != 0)
        std::cerr << "reader.currentFrame pre mismatch: " << reader.getCurrentFrame() << " != " << 0 << std::endl;

      std::vector<std::tuple<float>> stream(objects.size());

      for (uint32_t i = 0; i < reader.getNumberOfFrames(); i++)
      {
        float v1 = std::sin(i + 1);
        float v2 = std::sin(i - 1);

        if (!reader.getValues(stream, log))
          std::cerr << "getValues error: " << log.str() << std::endl;

        if (reader.getCurrentFrame() != i)
          std::cerr << "reader.currentFrame mismatch: " << reader.getCurrentFrame() << " != " << i << std::endl;

        if (std::get<0>(stream[0]) != v1)
          std::cerr << "value1 mismatch: " << std::get<0>(stream[0]) << " != " << v1 << std::endl;
        if (std::get<0>(stream[1]) != v2)
          std::cerr << "value2 mismatch: " << std::get<0>(stream[0]) << " != " << v2 << std::endl;

        if (i + 1 < reader.getNumberOfFrames())
          if (!reader.advanceFrames())
            std::cerr << "advanceFrames error" << std::endl;
      }
    }
  }
}

void testSinus3()
{
  std::vector<uint32_t> loops({ 1, 10, 100, 1000, 10000, 100000, 1000000 });
  std::stringstream log;

  for (uint32_t l : loops)
  {
    {
      std::vector<std::string> objects({ "o" });

      framerecorder::writer::RecordWriter<float, float> writer(objects, 73);

      std::vector<std::tuple<float, float>> stream(objects.size());

      for (uint32_t i = 0; i < l; i++)
      {
        float v1 = std::sin(i + 1);
        float v2 = std::sin(i - 1);

        if (writer.getNumberOfFrames() != i)
          std::cerr << "writer.numberOfFrames mismatch: " << writer.getNumberOfFrames() << " != " << i << std::endl;

        std::get<0>(stream[0]) = v1;
        std::get<1>(stream[0]) = v2;
        if (!writer.setValues(stream, log))
          std::cerr << "setValues error: " << log.str() << std::endl;
      }

      if (writer.getNumberOfFrames() != l)
        std::cerr << "writer.numberOfFrames mismatch: " << writer.getNumberOfFrames() << " != " << l << std::endl;

      if (!writer.save("test.bin"))
        std::cerr << "save error" << std::endl;
    }
    {
      framerecorder::reader::RecordReader<float, float> reader;

      if (!reader.load("test.bin", log))
        std::cerr << "load error: " << log.str() << std::endl;

      if (reader.getNumberOfFrames() != l)
        std::cerr << "reader.numberOfFrames mismatch: " << reader.getNumberOfFrames() << " != " << l << std::endl;

      if (reader.getFrameRate() != 73)
        std::cerr << "reader.frameRate mismatch: " << reader.getFrameRate() << " != " << 73 << std::endl;

      std::vector<std::string> objects = reader.getObjects();

      if (objects.size() != 1)
        std::cerr << "objects.size mismatch: " << objects.size() << " != " << 1 << std::endl;

      if (reader.getCurrentFrame() != 0)
        std::cerr << "reader.currentFrame pre mismatch: " << reader.getCurrentFrame() << " != " << 0 << std::endl;

      std::vector<std::tuple<float, float>> stream(objects.size());

      for (uint32_t i = 0; i < reader.getNumberOfFrames(); i++)
      {
        float v1 = std::sin(i + 1);
        float v2 = std::sin(i - 1);

        if (!reader.getValues(stream, log))
          std::cerr << "getValues error: " << log.str() << std::endl;

        if (reader.getCurrentFrame() != i)
          std::cerr << "reader.currentFrame mismatch: " << reader.getCurrentFrame() << " != " << i << std::endl;

        if (std::get<0>(stream[0]) != v1)
          std::cerr << "value mismatch1: " << std::get<0>(stream[0]) << " != " << v1 << std::endl;
        if (std::get<1>(stream[0]) != v2)
          std::cerr << "value mismatch2: " << std::get<1>(stream[0]) << " != " << v2 << std::endl;

        if (i + 1 < reader.getNumberOfFrames())
          if (!reader.advanceFrames())
            std::cerr << "advanceFrames error" << std::endl;
      }
    }
  }
}

struct Vector3
{
  float x, y, z;

  operator ==(const Vector3 & v)
  {
    return x == v.x && y == v.y && z == v.z;
  }
  operator !=(const Vector3 & v)
  {
    return !(*this == v);
  }
};

void testSinus4()
{
  std::vector<uint32_t> loops({ 1, 10, 100, 1000, 10000, 100000, 1000000 });
  std::stringstream log;

  for (uint32_t l : loops)
  {
    {
      std::vector<std::string> objects({ "o" });

      framerecorder::writer::RecordWriter<Vector3> writer(objects, 73);

      std::vector<std::tuple<Vector3>> stream(objects.size());

      for (uint32_t i = 0; i < l; i++)
      {
        float v1 = std::sin(i + 1);
        float v2 = std::sin(i - 1);
        float v3 = std::sin(i * 2);

        if (writer.getNumberOfFrames() != i)
          std::cerr << "writer.numberOfFrames mismatch: " << writer.getNumberOfFrames() << " != " << i << std::endl;

        std::get<0>(stream[0]).x = v1;
        std::get<0>(stream[0]).y = v2;
        std::get<0>(stream[0]).z = v3;
        if (!writer.setValues(stream, log))
          std::cerr << "setValues error: " << log.str() << std::endl;
      }

      if (writer.getNumberOfFrames() != l)
        std::cerr << "writer.numberOfFrames mismatch: " << writer.getNumberOfFrames() << " != " << l << std::endl;

      if (!writer.save("test.bin"))
        std::cerr << "save error" << std::endl;
    }
    {
      framerecorder::reader::RecordReader<Vector3> reader;

      if (!reader.load("test.bin", log))
        std::cerr << "load error: " << log.str() << std::endl;

      if (reader.getNumberOfFrames() != l)
        std::cerr << "reader.numberOfFrames mismatch: " << reader.getNumberOfFrames() << " != " << l << std::endl;

      if (reader.getFrameRate() != 73)
        std::cerr << "reader.frameRate mismatch: " << reader.getFrameRate() << " != " << 73 << std::endl;

      std::vector<std::string> objects = reader.getObjects();

      if (objects.size() != 1)
        std::cerr << "objects.size mismatch: " << objects.size() << " != " << 1 << std::endl;

      if (reader.getCurrentFrame() != 0)
        std::cerr << "reader.currentFrame pre mismatch: " << reader.getCurrentFrame() << " != " << 0 << std::endl;

      std::vector<std::tuple<Vector3>> stream(objects.size());

      for (uint32_t i = 0; i < reader.getNumberOfFrames(); i++)
      {
        float v1 = std::sin(i + 1);
        float v2 = std::sin(i - 1);
        float v3 = std::sin(i * 2);

        if (!reader.getValues(stream, log))
          std::cerr << "getValues error: " << log.str() << std::endl;

        if (reader.getCurrentFrame() != i)
          std::cerr << "reader.currentFrame mismatch: " << reader.getCurrentFrame() << " != " << i << std::endl;

        if (std::get<0>(stream[0]).x != v1)
          std::cerr << "value mismatch1: " << std::get<0>(stream[0]).x << " != " << v1 << std::endl;
        if (std::get<0>(stream[0]).y != v2)
          std::cerr << "value mismatch2: " << std::get<0>(stream[0]).y << " != " << v2 << std::endl;
        if (std::get<0>(stream[0]).z != v3)
          std::cerr << "value mismatch3: " << std::get<0>(stream[0]).z << " != " << v3 << std::endl;

        if (i + 1 < reader.getNumberOfFrames())
          if (!reader.advanceFrames())
            std::cerr << "advanceFrames error" << std::endl;
      }
    }
  }
}

void testSinus5()
{
  std::vector<uint32_t> loops({ 1, 10, 100, 1000, 10000, 100000, 1000000 });
  std::stringstream log;

  for (uint32_t l : loops)
  {
    {
      std::vector<std::string> objects({ "o" });

      framerecorder::writer::RecordWriter<float, float> writer(objects, 73);

      std::vector<std::tuple<float, float>> stream(objects.size());

      for (uint32_t i = 0; i < l; i++)
      {
        float v1 = std::sin(i + 1);
        float v2 = std::sin(std::floor(i / 10));

        if (writer.getNumberOfFrames() != i)
          std::cerr << "writer.numberOfFrames mismatch: " << writer.getNumberOfFrames() << " != " << i << std::endl;

        std::get<0>(stream[0]) = v1;
        std::get<1>(stream[0]) = v2;
        if (!writer.setValues(stream, log))
          std::cerr << "setValues error: " << log.str() << std::endl;
      }

      if (writer.getNumberOfFrames() != l)
        std::cerr << "writer.numberOfFrames mismatch: " << writer.getNumberOfFrames() << " != " << l << std::endl;

      if (!writer.save("test.bin"))
        std::cerr << "save error" << std::endl;
    }
    {
      framerecorder::reader::RecordReader<float, float> reader;

      if (!reader.load("test.bin", log))
        std::cerr << "load error: " << log.str() << std::endl;

      if (reader.getNumberOfFrames() != l)
        std::cerr << "reader.numberOfFrames mismatch: " << reader.getNumberOfFrames() << " != " << l << std::endl;

      if (reader.getFrameRate() != 73)
        std::cerr << "reader.frameRate mismatch: " << reader.getFrameRate() << " != " << 73 << std::endl;

      std::vector<std::string> objects = reader.getObjects();

      if (objects.size() != 1)
        std::cerr << "objects.size mismatch: " << objects.size() << " != " << 1 << std::endl;

      if (reader.getCurrentFrame() != 0)
        std::cerr << "reader.currentFrame pre mismatch: " << reader.getCurrentFrame() << " != " << 0 << std::endl;

      std::vector<std::tuple<float, float>> stream(objects.size());

      for (uint32_t i = 0; i < reader.getNumberOfFrames(); i++)
      {
        float v1 = std::sin(i + 1);
        float v2 = std::sin(std::floor(i / 10));

        if (!reader.getValues(stream, log))
          std::cerr << "getValues error: " << log.str() << std::endl;

        if (reader.getCurrentFrame() != i)
          std::cerr << "reader.currentFrame mismatch: " << reader.getCurrentFrame() << " != " << i << std::endl;

        if (std::get<0>(stream[0]) != v1)
          std::cerr << "value mismatch1: " << std::get<0>(stream[0]) << " != " << v1 << std::endl;
        if (std::get<1>(stream[0]) != v2)
          std::cerr << "value mismatch2: " << std::get<1>(stream[0]) << " != " << v2 << std::endl;

        if (i + 1 < reader.getNumberOfFrames())
          if (!reader.advanceFrames())
            std::cerr << "advanceFrames error" << std::endl;
      }
    }
  }
}

void testStepFrame()
{
  framerecorder::detail::ByteVector data(framerecorder::writer::detail::MAX_LENGTH);
  framerecorder::reader::detail::Stream<float> stream;
  framerecorder::reader::detail::StreamView<float> streamView;

  uint32_t offset(0);

  framerecorder::detail::writeType<uint16_t>(data, 1, offset);
  framerecorder::detail::writeType<float>(data, 1.0f, offset);
  framerecorder::detail::writeType<uint16_t>(data, 1, offset);

  framerecorder::detail::writeType<uint16_t>(data, 2, offset);
  framerecorder::detail::writeType<float>(data, 2.0f, offset);
  framerecorder::detail::writeType<float>(data, 3.0f, offset);
  framerecorder::detail::writeType<uint16_t>(data, 1, offset);

  framerecorder::detail::writeType<uint16_t>(data, 1, offset);
  framerecorder::detail::writeType<float>(data, 4.0f, offset);
  framerecorder::detail::writeType<uint16_t>(data, 2, offset);

  framerecorder::detail::writeType<uint16_t>(data, 2, offset);
  framerecorder::detail::writeType<float>(data, 5.0f, offset);
  framerecorder::detail::writeType<float>(data, 6.0f, offset);
  framerecorder::detail::writeType<uint16_t>(data, 2, offset);

  framerecorder::detail::writeType<uint16_t>(data, 1, offset);
  framerecorder::detail::writeType<float>(data, 7.0f, offset);
  framerecorder::detail::writeType<uint16_t>(data, 1, offset);

  stream.length = offset;
  stream.offset = 0;

  streamView.position = sizeof(uint16_t);
  streamView.repeats = 1;
  streamView.remaining = 1;
  streamView.stream = &stream;

  auto testPos = [&streamView, &data](uint16_t step, bool res, uint32_t pos)
  {
    static int testNum = 0;
    if (framerecorder::reader::detail::stepFrames(data, streamView, step) != res)
      std::cerr << "stepFrames fail " << testNum << ": actual=" << !res << ", expected=" << res << std::endl;
    if (streamView.position != pos)
      std::cerr << "streamView.position mismatch " << testNum << ": actual=" << streamView.position << ", expected=" << pos << std::endl;
    testNum++;
  };

  testPos(0,  true, sizeof(uint16_t) *  1 + sizeof(float) *  0);
  testPos(0,  true, sizeof(uint16_t) *  1 + sizeof(float) *  0);
  testPos(1,  true, sizeof(uint16_t) *  3 + sizeof(float) *  1);
  testPos(1,  true, sizeof(uint16_t) *  3 + sizeof(float) *  2);
  testPos(1,  true, sizeof(uint16_t) *  5 + sizeof(float) *  3);
  testPos(1,  true, sizeof(uint16_t) *  5 + sizeof(float) *  3);
  testPos(1,  true, sizeof(uint16_t) *  7 + sizeof(float) *  4);
  testPos(1,  true, sizeof(uint16_t) *  7 + sizeof(float) *  5);
  testPos(1,  true, sizeof(uint16_t) *  7 + sizeof(float) *  5);
  testPos(1,  true, sizeof(uint16_t) *  9 + sizeof(float) *  6);
  testPos(1, false, sizeof(uint16_t) *  9 + sizeof(float) *  6);

//  std::cerr << " HdrTEst" << std::endl;

  offset = 0;
  streamView.repeats = 1;
  streamView.remaining = 1;
  streamView.position = sizeof(uint16_t);

  framerecorder::detail::writeType<uint16_t>(data, 1, offset);
  framerecorder::detail::writeType<float>(data, 0.0f, offset);
  framerecorder::detail::writeType<uint16_t>(data, 1, offset);

  framerecorder::detail::writeType<uint16_t>(data, 0, offset);
  framerecorder::detail::writeType<uint16_t>(data, 1, offset);

  framerecorder::detail::writeType<uint16_t>(data, 1, offset);
  framerecorder::detail::writeType<float>(data, 0.0f, offset);
  framerecorder::detail::writeType<uint16_t>(data, 1, offset);

  framerecorder::detail::writeType<uint16_t>(data, 0, offset);
  framerecorder::detail::writeType<uint16_t>(data, 0, offset);

  framerecorder::detail::writeType<uint16_t>(data, 1, offset);
  framerecorder::detail::writeType<float>(data, 9.0f, offset);
  framerecorder::detail::writeType<uint16_t>(data, 1, offset);

  framerecorder::detail::writeType<uint16_t>(data, 1, offset);
  framerecorder::detail::writeType<float>(data, 9.0f, offset);
  framerecorder::detail::writeType<uint16_t>(data, 0, offset);

  framerecorder::detail::writeType<uint16_t>(data, 1, offset);
  framerecorder::detail::writeType<float>(data, 9.0f, offset);
  framerecorder::detail::writeType<uint16_t>(data, 1, offset);

  stream.length = offset;

  testPos(0,  true, sizeof(uint16_t) *   1 + sizeof(float) *  0); // 11
  testPos(1,  true, sizeof(uint16_t) *   5 + sizeof(float) *  1);
  testPos(1,  true, sizeof(uint16_t) *   9 + sizeof(float) *  2);
  testPos(1,  true, sizeof(uint16_t) *  13 + sizeof(float) *  4);

}

void testRecordBreak()
{
  auto testValue = [](framerecorder::reader::RecordReader<short> & reader,  std::vector<std::tuple<short>> & stream, std::stringstream & log, short expectedValue)
  {
    static int testNum = 0;
    if (!reader.getValues(stream, log))
      std::cerr << "getValues fail" << std::endl;
    if (std::get<0>(stream[0]) != expectedValue)
      std::cerr << "expectedValue mismatch " << testNum << ": actual=" << std::get<0>(stream[0]) << ", expected=" << expectedValue << std::endl;
//    else
//      std::cerr << "got value: " << std::get<0>(stream[0]) << std::endl;


    reader.advanceFrames();

    testNum++;
  };

  {
    std::vector<std::string> objects({ "o" });

    framerecorder::writer::RecordWriter<short> writer(objects, 1);

    std::vector<std::tuple<short>> stream(objects.size());
    std::stringstream log;

    std::get<0>(stream[0]) = 0;
    writer.setValues(stream, log);
    std::get<0>(stream[0]) = 1;
    writer.setValues(stream, log);
    std::get<0>(stream[0]) = 2;
    writer.setValues(stream, log, true);
    std::get<0>(stream[0]) = 3;
    writer.setValues(stream, log);
    std::get<0>(stream[0]) = 4;
    writer.setValues(stream, log, true);
    std::get<0>(stream[0]) = 5;
    writer.setValues(stream, log);
    std::get<0>(stream[0]) = 6;
    writer.setValues(stream, log);

    if (!writer.save("test.bin"))
      std::cerr << "save error" << std::endl;

    framerecorder::reader::RecordReader<short> reader;

    reader.getValues(stream, log);

    reader.load("test.bin", log);

    testValue(reader, stream, log, 0);
    testValue(reader, stream, log, 1);
    testValue(reader, stream, log, 2);
    testValue(reader, stream, log, 3);
    testValue(reader, stream, log, 4);
    testValue(reader, stream, log, 5);
    testValue(reader, stream, log, 6);
  }
  {
    std::vector<std::string> objects({ "o" });

    framerecorder::writer::RecordWriter<short> writer(objects, 1);

    std::vector<std::tuple<short>> stream(objects.size());
    std::stringstream log;

    std::get<0>(stream[0]) = 0;
    writer.setValues(stream, log);
    std::get<0>(stream[0]) = 1;
    writer.setValues(stream, log);
    std::get<0>(stream[0]) = 2;
    writer.setValues(stream, log, true);
    std::get<0>(stream[0]) = 3;
    writer.setValues(stream, log);
    std::get<0>(stream[0]) = 3;
    writer.setValues(stream, log, true);
    std::get<0>(stream[0]) = 3;
    writer.setValues(stream, log);
    std::get<0>(stream[0]) = 6;
    writer.setValues(stream, log);

    if (!writer.save("test.bin"))
      std::cerr << "save error" << std::endl;

    framerecorder::reader::RecordReader<short> reader;

    reader.getValues(stream, log);

    reader.load("test.bin", log);

    testValue(reader, stream, log, 0);
    testValue(reader, stream, log, 1);
    testValue(reader, stream, log, 2);
    testValue(reader, stream, log, 3);
    testValue(reader, stream, log, 3);
    testValue(reader, stream, log, 3);
    testValue(reader, stream, log, 6);
  }
}


void concat_records()
{
  std::vector<std::string> objects({ "o" });
  std::stringstream log;

  framerecorder::writer::RecordWriter<float, float> writer(objects, 73);
  std::vector<std::tuple<float, float>> stream(objects.size());
  std::get<0>(stream[0]) = 3.0f;
  std::get<1>(stream[0]) = 7.0f;
  writer.setValues(stream, log);

  framerecorder::writer::RecordWriter<short> writer2(objects, 11);
  std::vector<std::tuple<short>> stream2(objects.size());
  std::get<0>(stream2[0]) = 5;
  writer2.setValues(stream2, log);

  framerecorder::detail::ByteVector data;
  uint32_t offset = 0;
  std::cerr << "data size: " << offset << std::endl;
  writer.save(data, offset);
  std::cerr << "data size: " << offset << std::endl;
  writer2.save(data, offset);
  std::cerr << "data size: " << offset << std::endl;

  framerecorder::reader::RecordReader<float, float> reader;
  framerecorder::reader::RecordReader<short> reader2;

  offset = 0;
  reader.load(data, offset, log);
  std::cerr << reader.describe() << std::endl;
  reader2.load(data, offset, log);
  std::cerr << reader2.describe() << std::endl;
}

int main(int argc, char *argv[])
{
  testStepFrame();
  testRecordBreak();
  testSinus1();
  testSinus1b();
  testSinus2();
  testSinus3();
  testSinus4();
  testSinus5();

  concat_records();

  return 0;
}
