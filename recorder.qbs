import qbs

CppApplication {
    consoleApplication: true
    files: [
        "FrameRecorder.hpp",
        "README.md",
        "RecordReader.hpp",
        "RecordWriter.hpp",
        "format.txt",
        "main.cpp",
    ]

    Group {     // Properties for the produced executable
        fileTagsFilter: product.type
        qbs.install: true
    }
    cpp.cxxLanguageVersion: "c++17"
}
