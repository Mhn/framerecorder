﻿using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationRecorder : MonoBehaviour
{
	public class Location
	{
		public Vector3 position { get; set; }
		public Quaternion rotation { get; set; }
	}

	[SerializeField] private ushort m_frameRate = 60;

	private FrameRecorder.RecordWriter<Location> m_writer = null;
	private Location[] m_values = null;
	private Transform[] m_objects = null;
	private bool m_recording = false;

	void Start()
	{
		string log;

		m_objects = transform.GetComponentsInChildren<Transform>();

		string[] objectNames = new string[m_objects.Length];
		for (int i = 0; i < m_objects.Length; i++)
			objectNames[i] = GetRelativePath(m_objects[i]);

		m_writer = new FrameRecorder.RecordWriter<Location>(objectNames, m_frameRate);

		m_values = new Location[m_objects.Length];
		for (int o = 0; o < m_values.Length; o++)
			m_values[o] = new Location();
	}

	public string GetRelativePath(Transform current)
	{
		if (current == transform)
			return "";
		return GetRelativePath(current.parent) + current.name + "/";
	}

	public void BeginRecording()
	{
		if (!m_recording)
		{
			Debug.Log("Beginning recording.");
			m_recording = true;
			StartCoroutine(Record());
		}
	}

	public void EndRecording(string filename)
	{
		if (m_recording)
		{
			Debug.Log("Ending recording.");
			m_recording = false;
			m_writer.Save(filename);
		}
	}

	private IEnumerator Record()
	{
		string log;
		while (m_recording)
		{
			for (int i = 0; i < m_objects.Length; i++)
			{
				if (m_objects[i] != null)
				{
					m_values[i].position = m_objects[i].localPosition;
					m_values[i].rotation = m_objects[i].localRotation;
				}
			}
			m_writer.SetValues(ref m_values, out log);
			yield return new WaitForSeconds(1.0f / m_frameRate);
		}
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.F12))
		{
			if (!m_recording)
				BeginRecording();
			else
				EndRecording("locations.bin");
		}
	}
}
