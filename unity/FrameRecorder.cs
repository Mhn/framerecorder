﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System.Runtime.InteropServices;

namespace FrameRecorder
{
	public class RecordReader<T> where T : class
	{
		private byte[] m_data = null;
		private PropertyInfo[] m_fields = null;
		private Reader.Detail.Record[] m_records;
		private Reader.Detail.RecordView m_recordView;

		public string[] Objects { get; private set; }
		public UInt16 FrameRate { get; private set; }
		public UInt32 NumberOfFrames { get; private set; }
		public UInt32 CurrentFrame { get; private set; }

		public RecordReader()
		{
			m_fields = typeof(T).GetProperties();
		}

		public bool LoadFile(string filename, out string log)
		{
			m_data = System.IO.File.ReadAllBytes(filename);
			return Load(out log);
		}

		public bool LoadResource(string resource, out string log)
		{
			TextAsset textAsset = Resources.Load(resource) as TextAsset;
			m_data = textAsset.bytes;
			return Load(out log);
		}

		private bool Load(out string log)
		{
			UInt32 offset = 0;

			if (Reader.Detail.ReadString(m_data, ref offset, Reader.Detail.MagicString.Length) != Reader.Detail.MagicString) {
				log = "File Magic string match fail";
				return false;
			}

			UInt16 version = Reader.Detail.ReadUint16(m_data, ref offset);

			if (version != 1)
			{
				log = "Unsupported file version: " + version;
				return false;
			}

			FrameRate = Reader.Detail.ReadUint16(m_data, ref offset);
			NumberOfFrames = Reader.Detail.ReadUint32(m_data, ref offset);

			UInt16 numRecords = Reader.Detail.ReadUint16(m_data, ref offset);
			UInt16 numStreams = Reader.Detail.ReadUint16(m_data, ref offset);
			UInt16 numObjects = Reader.Detail.ReadUint16(m_data, ref offset);

			if (numStreams != m_fields.Length)
			{
				log = "Mismatching number of streams. Expected: " + m_fields.Length + ", contained in file: " + numStreams;
				return false;
			}

			Objects = new string[numObjects];

			for (int i = 0; i < Objects.Length; i++)
			{
				UInt16 strlen = Reader.Detail.ReadUint16(m_data, ref offset);
				Objects[i] = Reader.Detail.ReadString(m_data, ref offset, strlen);
			}

			m_records = new Reader.Detail.Record[numRecords];

			for (int r = 0; r < numRecords; r++)
			{
				m_records[r].numFrames = Reader.Detail.ReadUint32(m_data, ref offset);
				m_records[r].entries = new Reader.Detail.Entry[numObjects];

				for (int e = 0; e < numObjects; e++)
				{
					m_records[r].entries[e].streams = new Reader.Detail.Stream[numStreams];

					for (int s = 0; s < numStreams; s++)
					{
						m_records[r].entries[e].streams[s].length = Reader.Detail.ReadUint16 (m_data, ref offset);
						m_records[r].entries[e].streams[s].offset = offset;
						offset += m_records[r].entries[e].streams[s].length;
					}
				}
			}

			m_recordView.entryViews = new Reader.Detail.EntryView[numObjects];
			for (int e = 0; e < numObjects; e++)
				m_recordView.entryViews[e].streamViews = new Reader.Detail.StreamView[numStreams];

			SetFrame(0);

			log = "Successfully loaded file";
			return true;
		}

		public bool GetValues(ref T[] values, out string log)
		{
			if (values.Length != Objects.Length)
			{
				log = "Error: Number of values does not equal number of objects.";
				return false;
			}
			//Debug.Log("Current frame: " + CurrentFrame);

			for (UInt16 i = 0; i < values.Length; i++)
			{
				for (int s = 0; s < m_recordView.entryViews[i].streamViews.Length; s++)
				{
					UInt32 pos = m_recordView.entryViews[i].streamViews[s].position;

					IntPtr bufPos = Marshal.UnsafeAddrOfPinnedArrayElement(m_data, (int)pos);

					m_fields[s].SetValue(values[i], Marshal.PtrToStructure(bufPos, m_fields[s].PropertyType), null);
					//Debug.Log("Read value: " + Marshal.PtrToStructure(bufPos, m_fields[s].PropertyType) + " pos: " + pos);
				}
			}
			log = "";
			return true;
		}

		public bool AdvanceFrames(UInt32 frames = 1)
		{
			UInt32 targetFrame = CurrentFrame + frames;
			UInt32 targetEntryFrame = m_recordView.entryFrame + frames;

			if (targetEntryFrame >= m_records[m_recordView.recordIndex].numFrames)
				return SetFrame(targetFrame);

			for (int e = 0; e < m_recordView.entryViews.Length; e++)
				for (int s = 0; s < m_recordView.entryViews[e].streamViews.Length; s++)
					Reader.Detail.StepFrames(m_data, ref m_recordView.entryViews[e].streamViews[s], frames, Marshal.SizeOf(m_fields[s].PropertyType));

			m_recordView.entryFrame = targetEntryFrame;
			CurrentFrame = targetFrame;

			return true;
		}

		public bool SetFrame(UInt32 frame)
		{
			if (frame < NumberOfFrames)
			{
				UInt32 f = frame;

				for (UInt16 i = 0; i < m_records.Length; i++)
				{
					if (f < m_records[i].numFrames)
					{
						m_recordView.recordIndex = i;
						m_recordView.entryFrame = 0;

						for (int e = 0; e < m_recordView.entryViews.Length; e++)
						{
							for (int s = 0; s < m_recordView.entryViews[e].streamViews.Length; s++)
							{
								//Debug.Log("Resetting stream");
								Reader.Detail.Reset(m_data, ref m_recordView.entryViews[e].streamViews[s], ref m_records[m_recordView.recordIndex].entries[e].streams[s], Marshal.SizeOf(m_fields[s].PropertyType));
							}
						}

						CurrentFrame = frame - f;

						return AdvanceFrames(f);
					}
					f -= m_records[i].numFrames;
				}
			}

			return false;
		}
			
	}







	public class RecordWriter<T> where T : class, new()
	{

		public string[] Objects { get; private set; }
		public UInt16 FrameRate { get; private set; }
		public UInt32 NumberOfFrames { get; private set; }

		private PropertyInfo[] m_fields = null;
		private Writer.Detail.Record<T>[] m_records;

		public RecordWriter(string[] objects, UInt16 frameRate)
		{
			FrameRate = frameRate;
			Objects = objects;
			m_fields = typeof(T).GetProperties();

			m_records = new FrameRecorder.Writer.Detail.Record<T>[1];

			Writer.Detail.InitializeRecord(ref m_records[0], Objects.Length, m_fields.Length);
		}

		public bool SetValues(ref T[] values, out string log)
		{
			if (values.Length != Objects.Length)
			{
				log = "Error: Number of values does not equal number of objects.";
				return false;
			}
				
			for (UInt16 i = 0; i < values.Length; i++)
			{
				for (int s = 0; s < m_fields.Length; s++)
				{
					var val = m_records[m_records.Length - 1].entries[i].lastValue == null ? null : m_fields[s].GetValue(m_records[m_records.Length - 1].entries[i].lastValue, null);
					Writer.Detail.writeFrame(ref m_records[m_records.Length - 1].entries[i].streams[s], m_fields[s].GetValue(values[i], null), val);

					m_fields[s].SetValue(m_records[m_records.Length - 1].entries[i].lastValue, m_fields[s].GetValue(values[i], null), null);
				}
			}
			log = "";

			return AdvanceFrame();
		}

		public bool Save(string filename)
		{
			FinalizeRecord();

			byte[] data = new byte[CalculateBufSize()];

			UInt32 offset = 0;

			Writer.Detail.WriteString(data, Reader.Detail.MagicString, ref offset);
			Writer.Detail.WriteUint16(data, 1, ref offset); // version
			Writer.Detail.WriteUint16(data, FrameRate, ref offset);
			Writer.Detail.WriteUint32(data, NumberOfFrames, ref offset);
			Writer.Detail.WriteUint16(data, (UInt16)m_records.Length, ref offset);
			Writer.Detail.WriteUint16(data, (UInt16)m_fields.Length, ref offset);
			Writer.Detail.WriteUint16(data, (UInt16)Objects.Length, ref offset);

			for (int i = 0; i < Objects.Length; i++)
			{
				Writer.Detail.WriteUint16(data, (UInt16)Objects[i].Length, ref offset);
				Writer.Detail.WriteString(data, Objects[i], ref offset);
			}

			for (int r = 0; r < m_records.Length; r++)
			{
				Writer.Detail.WriteUint32(data, m_records[r].numFrames, ref offset);

				for (int e = 0; e < m_records[r].entries.Length; e++)
				{
					for (int s = 0; s < m_records[r].entries[e].streams.Length; s++)
					{
						Writer.Detail.WriteUint16(data, m_records[r].entries[e].streams[s].position, ref offset);
						Writer.Detail.WriteArray(data, m_records[r].entries[e].streams[s].data, ref offset);
					}
				}
			}

			System.IO.File.WriteAllBytes(filename, data);

			return true;
		}

		private void FinalizeRecord()
		{
			for (UInt16 e = 0; e < m_records[m_records.Length - 1].entries.Length; e++)
			{
				for (UInt16 s = 0; s < m_records[m_records.Length - 1].entries[e].streams.Length; s++)
				{
					Writer.Detail.Finalize(ref m_records[m_records.Length - 1].entries[e].streams[s]);
					Array.Resize(ref m_records[m_records.Length - 1].entries[e].streams[s].data, m_records[m_records.Length - 1].entries[e].streams[s].position);
					m_records[m_records.Length - 1].entries[e].length += (UInt32)m_records[m_records.Length - 1].entries[e].streams[s].position + (UInt32)sizeof(UInt16);
				}
				m_records[m_records.Length - 1].length += m_records[m_records.Length - 1].entries[e].length;
			}
			//NumberOfFrames += m_records[m_records.Length - 1].numFrames;
		}

		private bool AdvanceFrame()
		{
			bool endOfStream = false;

			for (UInt16 e = 0; e < m_records[m_records.Length - 1].entries.Length; e++)
			{
				for (UInt16 s = 0; s < m_records[m_records.Length - 1].entries[e].streams.Length; s++)
				{
					Writer.Detail.CheckStreamAtEnd(ref m_records[m_records.Length - 1].entries[e].streams[s], Marshal.SizeOf(m_fields[s].PropertyType), ref endOfStream);
				}
				m_records[m_records.Length - 1].length += m_records[m_records.Length - 1].entries[e].length;
			}

			m_records[m_records.Length - 1].numFrames++;
			NumberOfFrames++;

			if (endOfStream)
			{
				FinalizeRecord();

				Array.Resize(ref m_records, m_records.Length + 1);

				Writer.Detail.InitializeRecord(ref m_records[m_records.Length - 1], Objects.Length, m_fields.Length);
			}

			return true;
		}

		private UInt32 CalculateBufSize()
		{
			UInt32 bufsize = (UInt32)Reader.Detail.MagicString.Length;

			bufsize += 5 * sizeof(UInt16);
			bufsize += 1 * sizeof(UInt32);

			for (int i = 0; i < Objects.Length; i++)
				bufsize += (UInt32)(Objects[i].Length + sizeof(UInt16));

			for (int i = 0; i < m_records.Length; i++)
				bufsize += m_records[i].length + sizeof(UInt32);
			
			return bufsize;
		}

	}


	namespace Writer {

		public static class Detail
		{
			public static UInt16 MaxLength = UInt16.MaxValue;

			public struct Stream {
				public byte[] data;
				public UInt16 position;
				public UInt16 hdrUniquePos;
				public UInt16 numRepeats;
				public UInt16 numUnique;
			};

			public struct Entry<T> {
				public Stream[] streams;
				public T lastValue;
				public UInt32 length;
			};

			public struct Record<T> {
				public Entry<T>[] entries;
				public UInt32 length;
				public UInt32 numFrames;
			};

			public static void InitializeRecord<T>(ref Record<T> record, int numEntries, int numStreams) where T : class, new()
			{
				record.entries = new FrameRecorder.Writer.Detail.Entry<T>[numEntries];
				for (int e = 0; e < numEntries; e++)
				{
					record.entries[e].streams = new Stream[numStreams];
					record.entries[e].lastValue = new T();

					for (int s = 0; s < numStreams; s++)
					{
						record.entries[e].streams[s].data = new byte[MaxLength];
						record.entries[e].streams[s].position = sizeof(UInt16);
					}
				}
			}

			public static void WriteUint16(byte[] data, UInt16 v, ref UInt32 offset)
			{
				BitConverter.GetBytes(v).CopyTo(data, offset);

				offset += (UInt32)sizeof(UInt16);
			}

			public static void WriteUint16(byte[] data, UInt16 v, ref UInt16 offset)
			{
				BitConverter.GetBytes(v).CopyTo(data, offset);

				offset += (UInt16)sizeof(UInt16);
			}

			public static void WriteUint32(byte[] data, UInt32 v, ref UInt32 offset)
			{
				BitConverter.GetBytes(v).CopyTo(data, offset);

				offset += (UInt32)sizeof(UInt32);
			}

			public static void WriteArray(byte[] data, byte[] v, ref UInt32 offset)
			{
				v.CopyTo(data, offset);
				offset += (UInt32)v.Length;
			}

			public static void WriteString(byte[] data, string s, ref UInt32 offset)
			{
				System.Text.Encoding.ASCII.GetBytes(s, 0, s.Length, data, (int)offset);
				offset += (UInt32)s.Length;
			}

			public static void writeFrame<T>(ref Stream stream, T value, T lastValue)
			{
				bool first = stream.hdrUniquePos + sizeof(UInt16) == stream.position;
				bool unique = first ? true : !Marshal.Equals(lastValue, value);
				
				bool lastUnique = stream.numRepeats == 0;

				bool newSection = (unique && !lastUnique && !first) ||
					stream.numUnique == UInt16.MaxValue ||
					stream.numRepeats == UInt16.MaxValue;

				// If we need a new header
				if (newSection)
				{
					Finalize(ref stream);
					WriteUint16(stream.data, 0, ref stream.position);
				}

				// first frame in entry, or different values
				if (unique || first)
				{
					//Debug.Log("Writing value: " + value);
					IntPtr bufPos = Marshal.UnsafeAddrOfPinnedArrayElement(stream.data, (int)stream.position);
					Marshal.StructureToPtr(value, bufPos, true);
					stream.position += (UInt16)Marshal.SizeOf(value);
					stream.numUnique++;
				}
				else
				{
					stream.numRepeats++;
				}
			}

			public static void Finalize(ref Stream stream)
			{
				UInt32 pos = stream.hdrUniquePos;
				WriteUint16(stream.data, stream.numUnique, ref pos);
				UInt16 numReps = (UInt16)(stream.numRepeats + (UInt16)((stream.numUnique > (UInt16)0) ? (UInt16)1 : (UInt16)0)); // wtf c#?
				WriteUint16(stream.data, numReps, ref stream.position);
				stream.numUnique = stream.numRepeats = 0;
				stream.hdrUniquePos = stream.position;
			}

			public static void CheckStreamAtEnd(ref Stream stream, int valueSize, ref bool endOfStream)
			{
				if (stream.position + valueSize + valueSize + sizeof(UInt16) > MaxLength)
					endOfStream = true;
			}
		}

	}








	namespace Reader {
		
		public static class Detail
		{
			public struct Stream {
				public UInt32 offset;
				public UInt16 length;
			};

			public struct Entry {
				public Stream[] streams;
			};

			public struct Record {
				public Entry[] entries;
				public UInt32 numFrames;
			};


			public struct StreamView
			{
				public UInt32 position;
				public UInt16 repeats;
				public UInt16 remaining;
				public Stream stream;
			};

			public struct EntryView
			{
				public StreamView[] streamViews;
			};

			public struct RecordView
			{
				public UInt16 recordIndex;
				public UInt32 entryFrame;
				public EntryView[] entryViews;
			};


			public static string MagicString = "FREC";

			public static UInt16 ReadUint16(byte[] data, ref UInt32 offset)
			{
				UInt16 v = BitConverter.ToUInt16(data, (int)offset);
				offset += sizeof(UInt16);
				return v;
			}

			public static UInt32 ReadUint32(byte[] data, ref UInt32 offset)
			{
				UInt32 v = BitConverter.ToUInt32(data, (int)offset);
				offset += sizeof(UInt32);
				return v;
			}

			public static string ReadString(byte[] data, ref UInt32 offset, int length)
			{
				string v = System.Text.Encoding.ASCII.GetString(data, (int)offset, length);
				offset += (UInt32)length;
				return v;
			}

			public static void Reset(byte[] data, ref StreamView streamView, ref Stream stream, int valueSize)
			{
				streamView.stream = stream;
				streamView.position = stream.offset;
				streamView.remaining = ReadUint16(data, ref streamView.position);
				UInt32 repeatHdrPos = streamView.position + (UInt32)(streamView.remaining * (UInt16)valueSize);
				streamView.repeats = ReadUint16(data, ref repeatHdrPos);
			}

			public static bool StepFrames(byte[] data, ref StreamView streamView, UInt32 frameNumber, int valueSize)
			{
				do
				{
					if (frameNumber > 0)
					{
						if (streamView.remaining > 1)
						{
							// step forward individual frames
							streamView.position += (UInt32)(valueSize);
							frameNumber--;
							streamView.remaining--;
						}
						else if (streamView.repeats > 0)
						{
							// step forward repeating values
							frameNumber--;
							streamView.repeats--;
						}
					}

					while (streamView.remaining < 1 || streamView.repeats < 1)
					{
						// read next header
						UInt32 nextPos = streamView.position + (UInt32)valueSize * streamView.remaining + (UInt32)sizeof(UInt16);

						if (nextPos >= streamView.stream.offset + streamView.stream.length)
							return false;
						
						streamView.position = nextPos;
						streamView.remaining = ReadUint16(data, ref streamView.position);
						UInt32 repeatHdrPos = streamView.position + (UInt32)(streamView.remaining * (UInt16)valueSize);
						streamView.repeats = ReadUint16(data, ref repeatHdrPos);
					}
				}
				while (frameNumber > 0);

				return true;
			}
		}

	}
}
