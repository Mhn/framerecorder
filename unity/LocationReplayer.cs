﻿using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationReplayer : MonoBehaviour
{
	public class Location
	{
		public Vector3 position { get; set; }
		public Quaternion rotation { get; set; }
	}

	[SerializeField] private string m_filename = null;
	[SerializeField] private bool m_loop = false;

	[Tooltip("Interpolate the position and orientation during this time in the beginning of the replay.")]
	[SerializeField] private float m_easeInTime = 0.0f;

	[Tooltip("Preload the first frame of the replay on start.")]
	[SerializeField] private bool m_preloadReplay = false;

	private FrameRecorder.RecordReader<Location> m_reader = null;
	private Location[][] m_values = null;
	private Transform[] m_objects = null;
	private int m_currentValueIndex = 0;
	private int m_currentFrame = 0;
	private int m_step = 0;
	private float m_time = 0.0f;
	private string m_log;

	public bool Replay { get; set; }
	
	void Start()
	{
		if (!string.IsNullOrEmpty(m_filename))
			LoadFile(m_filename);
	}

	public void LoadFile(string filename)
	{
		m_reader = new FrameRecorder.RecordReader<Location>();

		if (!m_reader.LoadFile(filename, out m_log))
			Debug.Log("reader.Load: " + m_log);
		else
		{
			string[] objects = m_reader.Objects;

			m_objects = new Transform[objects.Length];
			for (int i = 0; i < m_objects.Length; i++)
				m_objects[i] = transform.Find(objects[i]);

			m_values = new Location[2][];
			for (int i = 0; i < 2; i++)
			{
				m_values[i] = new Location[objects.Length];
				for (int o = 0; o < m_values[i].Length; o++)
					m_values[i][o] = new Location();
			}

			Debug.Log("Objects: " + objects.Length + " Framerate " + m_reader.FrameRate + " numFrames " + m_reader.NumberOfFrames);

			Reset();
		}
	}

	public void Reset()
	{
		// Preload replayer with first frame values
		m_reader.SetFrame(0);
		m_reader.GetValues(ref m_values[0], out m_log);
		m_reader.GetValues(ref m_values[1], out m_log);
		m_currentValueIndex = 0;
		m_currentFrame = 0;
		m_time = 0.0f;

		if (m_preloadReplay)
		{
			for (int i = 0; i < m_objects.Length; i++)
			{
				if (m_objects[i] != null)
				{
					m_objects[i].localPosition = m_values[0][i].position;
					m_objects[i].localRotation = m_values[0][i].rotation;
				}
			}
		}
	}

	private void Update()
	{
		if (Replay)
		{
			int frame = (int)(m_time * m_reader.FrameRate);

			while (m_currentFrame < frame)
			{
				Debug.Log("Frame " + m_currentFrame);
				m_reader.GetValues(ref m_values[m_currentValueIndex++ % 2], out m_log);

				if (!m_reader.AdvanceFrames())
				{
					if (m_loop)
						Reset();
					else
						Replay = false;
					return;
				}
				m_currentFrame++;
			}

			float ratio = Mathf.Repeat(m_time, m_reader.FrameRate);
			int first = m_currentValueIndex % 2;
			int last = (m_currentValueIndex + 1) % 2;
			bool ease = m_easeInTime > 0.0f && m_time < m_easeInTime;
			float easeRatio = 1.0f - Mathf.Exp(- 25.0f / m_easeInTime * (m_time / m_easeInTime));
			//Debug.Log("easeRatio " + easeRatio);

			for (int i = 0; i < m_objects.Length; i++)
			{
				if (m_objects[i] != null)
				{
					Vector3 pos = Vector3.Lerp(m_values[first][i].position, m_values[last][i].position, ratio);
					Quaternion rot = Quaternion.Slerp(m_values[first][i].rotation, m_values[last][i].rotation, ratio);

					m_objects[i].localPosition = ease ? Vector3.Lerp(m_objects[i].localPosition, pos, easeRatio) : pos;
					m_objects[i].localRotation = ease ? Quaternion.Slerp(m_objects[i].localRotation, rot, easeRatio) : rot;
				}
			}
			//Replay = false;
			m_time += Time.deltaTime;
		}
		else if (m_step != 0)
		{
			int newFrame = (int)(m_reader.CurrentFrame + m_step) % (int)m_reader.NumberOfFrames;
			newFrame = newFrame < 0 ? newFrame + (int)m_reader.NumberOfFrames : newFrame;
			m_reader.SetFrame((uint)newFrame);
			m_step = 0;
			m_reader.GetValues(ref m_values[0], out m_log);
			for (int i = 0; i < m_objects.Length; i++)
			{
				if (m_objects[i] != null)
				{
					m_objects[i].localPosition = m_values[0][i].position;
					Debug.Log("pos: " + m_values[0][i].position + " rot: " + m_values[0][i].rotation);
					//m_objects[i].localRotation = m_values[0][i].rotation;
				}
			}
		}

		if (Input.GetKeyDown(KeyCode.F11))
			Replay = !Replay;
		else if (Input.GetKeyDown(KeyCode.PageDown))
			m_step++;
		else if (Input.GetKeyDown(KeyCode.PageUp))
			m_step--;
	}
}
