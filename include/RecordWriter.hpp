#pragma once

#include "FrameRecorder.hpp"

#include <limits>
#include <fstream>
#include <iterator>
#include <iostream>

namespace framerecorder { namespace writer {

  namespace detail {

    const uint16_t MAX_LENGTH = std::numeric_limits<uint16_t>::max();

    template <typename T>
    struct Stream
    {
      framerecorder::detail::ByteVector data = framerecorder::detail::ByteVector(MAX_LENGTH);
      uint16_t position = sizeof(uint16_t);
      uint16_t hdrUniquePos = 0;
      T lastvalue = {};
      uint16_t numRepeats = 0;
      uint16_t numUnique = 0;
    };

    template <typename... Ts>
    struct Entry
    {
      std::tuple<Stream<Ts>...> streams;
      uint32_t length = 0;
    };

    template <typename... Ts>
    struct Record
    {
      std::vector<Entry<Ts...>> entries;
      uint32_t length = 0;
      uint32_t numFrames = 0;
    };

    static bool writeAllBytes(std::string const & filename, framerecorder::detail::ByteVector const & data, uint32_t length)
    {
        std::ofstream file(filename, std::ios::out | std::ios::binary);
        if (!file.is_open())
          return false;
        std::copy(data.cbegin(), std::next(data.begin(), length), std::ostream_iterator<framerecorder::detail::Byte>(file));
        return true;
    }

    template <typename T>
    static inline void finalize(Stream<T> & stream)
    {
      uint32_t pos = stream.hdrUniquePos;

      framerecorder::detail::writeType<uint16_t>(stream.data, stream.numUnique, pos);
      framerecorder::detail::writeType<uint16_t>(stream.data, stream.numRepeats + (stream.numUnique > 0), stream.position);
      stream.numUnique = stream.numRepeats = 0;
      stream.hdrUniquePos = stream.position;
    }

    template <typename T>
    static inline void writeFrame(Stream<T> & stream, T const & value)
    {
      bool first = stream.hdrUniquePos + sizeof(uint16_t) == stream.position;

      bool unique = first ? true : (stream.lastvalue != value);

      stream.lastvalue = value;

      bool lastUnique = stream.numRepeats == 0;

      bool newSection = (unique && !lastUnique && !first) ||
          stream.numUnique == std::numeric_limits<uint16_t>::max() ||
          stream.numRepeats == std::numeric_limits<uint16_t>::max();

      // If we need a new header
      if (newSection)
      {
        finalize(stream);
        framerecorder::detail::writeType<uint16_t>(stream.data, 0, stream.position);
      }

      // first frame in entry, or different values
      if (unique || first)
      {
        framerecorder::detail::writeType<T>(stream.data, value, stream.position);
        stream.numUnique++;
      }
      else
        stream.numRepeats++;
    }

    template <typename T>
    static inline void checkStreamAtEnd(Stream<T> const & stream, bool & endOfStream)
    {
      if (stream.position + sizeof(T) + sizeof(T) + sizeof(uint16_t) > MAX_LENGTH)
        endOfStream = true;
    }

  }

  template <typename... Ts>
  class RecordWriter
  {
    public:

      RecordWriter(std::vector<std::string> const & objects, uint16_t frameRate)
        : m_frameRate(frameRate),
          m_objects(objects)
      {
        detail::Record<Ts...> record;
        record.entries.resize(objects.size());

        m_records.push_back(record);
      }

      bool setValues(std::vector<std::tuple<Ts...>> const & values, std::stringstream & log, bool forceNewRecord = false)
      {
        if (values.size() != m_objects.size())
        {
          log << "Error: Number of values does not equal number of objects.";
          return false;
        }

        for (uint16_t i = 0; i < values.size(); i++)
        {
          detail::Entry<Ts...> & entry = m_records.back().entries[i];

          framerecorder::detail::tuple_utils::for_each_stream(values[i], entry.streams, [](auto const &value, auto &stream) {
            detail::writeFrame(stream, value);
          });
        }

        return advanceFrame(forceNewRecord);
      }

      bool save(framerecorder::detail::ByteVector & data, uint32_t & offset)
      {
        finalize();

        uint32_t totSize(offset + calculateBufSize());
        if (data.size() < totSize)
          data.resize(totSize);

//        uint32_t offset(0);
        framerecorder::detail::writeContainer<std::string>(data, framerecorder::detail::MAGIC_STRING, offset);
        framerecorder::detail::writeType<uint16_t>(data, 1, offset);
        framerecorder::detail::writeType<uint16_t>(data, m_frameRate, offset);
        framerecorder::detail::writeType<uint32_t>(data, m_numFrames, offset);
        framerecorder::detail::writeType<uint16_t>(data, m_records.size(), offset);
        framerecorder::detail::writeType<uint16_t>(data, sizeof...(Ts), offset); // number of streams
        framerecorder::detail::writeType<uint16_t>(data, m_objects.size(), offset);

        for (std::string const & object : m_objects)
        {
          framerecorder::detail::writeType<uint16_t>(data, object.size(), offset);
          framerecorder::detail::writeContainer(data, object, offset);
        }

        for (const detail::Record<Ts...> & record : m_records)
        {
          framerecorder::detail::writeType<uint32_t>(data, record.numFrames, offset);

          for (const detail::Entry<Ts...> & entry : record.entries)
          {
            framerecorder::detail::tuple_utils::for_each_stream(entry.streams, [&data, &offset](auto &stream) {
              framerecorder::detail::writeType<uint16_t>(data, stream.position, offset);
              framerecorder::detail::writeContainer(data, stream.data, offset);
            });
          }
        }

        return true;
      }

      bool save(std::string const & filename)
      {
        framerecorder::detail::ByteVector data;
        uint32_t offset(0);

        if (!save(data, offset))
          return false;

        return detail::writeAllBytes(filename, data, data.size());
      }

      uint32_t getNumberOfFrames() { return m_numFrames; }

    private:

      bool advanceFrame(bool forceNewRecord)
      {
        bool endOfStream(forceNewRecord);

        for (detail::Entry<Ts...> const & entry : m_records.back().entries)
        {
          framerecorder::detail::tuple_utils::for_each_stream(entry.streams, [&endOfStream](auto const &stream) {
            detail::checkStreamAtEnd(stream, endOfStream);
          });
        }

        m_records.back().numFrames++;
        m_numFrames++;

        if (endOfStream)
        {
          finalize();
          m_records.resize(m_records.size() + 1);
          m_records.back().entries.resize(m_objects.size());
        }

        return true;
      }

      void finalize()
      {
        for (detail::Entry<Ts...> & entry : m_records.back().entries)
        {
          framerecorder::detail::tuple_utils::for_each_stream(entry.streams, [&entry](auto &stream) {
            detail::finalize(stream);
            stream.data.resize(stream.position);
            entry.length += stream.position + sizeof(uint16_t);
          });

          m_records.back().length += entry.length;
        }
      }

      uint32_t calculateBufSize()
      {
        uint32_t bufsize(framerecorder::detail::MAGIC_STRING.length());

        bufsize += 5 * sizeof(uint16_t);
        bufsize += 1 * sizeof(uint32_t);

        for (std::string const & object : m_objects)
          bufsize += object.size() + sizeof(uint16_t);

        for (const detail::Record<Ts...> & record : m_records)
          bufsize += record.length + sizeof(uint32_t);

        return bufsize;
      }

    private:
      uint16_t m_frameRate = 0;
      uint32_t m_numFrames = 0;
      std::vector<std::string> m_objects;
      std::vector<detail::Record<Ts...>> m_records;
  };

}}
