#pragma once

#include <string>
#include <type_traits>
#include <tuple>
#include <vector>
#include <cstring>

namespace framerecorder {

  namespace detail {

    const std::string MAGIC_STRING = "FREC";

    using Byte = char;
    using ByteVector = std::vector<Byte>;

    namespace tuple_utils {

      // std::tuple<A<Ts>...>

      template<class F, template <typename> class A, class...Ts, std::size_t...Is>
      void for_each_stream_impl(std::tuple<A<Ts>...> & streams, F func, std::index_sequence<Is...>){
          using expander = int[];
          (void)expander { 0, ((void)func(std::get<Is>(streams)), 0)... };
      }

      template<class F, template <typename> class A, class...Ts>
      void for_each_stream(std::tuple<A<Ts>...> & streams, F func){
          for_each_stream_impl(streams, func, std::make_index_sequence<sizeof...(Ts)>());
      }

      // std::tuple<A<Ts>...> const

      template<class F, template <typename> class A, class...Ts, std::size_t...Is>
      void for_each_stream_impl(std::tuple<A<Ts>...> const & streams, F func, std::index_sequence<Is...>){
          using expander = int[];
          (void)expander { 0, ((void)func(std::get<Is>(streams)), 0)... };
      }

      template<class F, template <typename> class A, class...Ts>
      void for_each_stream(std::tuple<A<Ts>...> const & streams, F func){
          for_each_stream_impl(streams, func, std::make_index_sequence<sizeof...(Ts)>());
      }

      // std::tuple<Ts...> const, std::tuple<A<Ts>...>

      template<class F, template <typename> class A, class...Ts, std::size_t...Is>
      void for_each_stream_impl(std::tuple<Ts...> const & values, std::tuple<A<Ts>...> & streams, F func, std::index_sequence<Is...>){
          using expander = int[];
          (void)expander { 0, ((void)func(std::get<Is>(values), std::get<Is>(streams)), 0)... };
      }

      template<class F, template <typename> class A, class...Ts>
      void for_each_stream(std::tuple<Ts...> const & values, std::tuple<A<Ts>...> & streams, F func){
          for_each_stream_impl(values, streams, func, std::make_index_sequence<sizeof...(Ts)>());
      }

      // std::tuple<Ts...>, std::tuple<A<Ts>...> const

      template<class F, template <typename> class A, class...Ts, std::size_t...Is>
      void for_each_stream_impl(std::tuple<Ts...> & values, std::tuple<A<Ts>...> const & streams, F func, std::index_sequence<Is...>){
          using expander = int[];
          (void)expander { 0, ((void)func(std::get<Is>(values), std::get<Is>(streams)), 0)... };
      }

      template<class F, template <typename> class A, class...Ts>
      void for_each_stream(std::tuple<Ts...> & values, std::tuple<A<Ts>...> const & streams, F func){
          for_each_stream_impl(values, streams, func, std::make_index_sequence<sizeof...(Ts)>());
      }

      // std::tuple<A<Ts>...>, const, std::tuple<B<Ts>...>

      template<class F, template <typename> class A, template <typename> class B, class...Ts, std::size_t...Is>
      void for_each_stream_impl(std::tuple<A<Ts>...> const & values, std::tuple<B<Ts>...> & streams, F func, std::index_sequence<Is...>){
          using expander = int[];
          (void)expander { 0, ((void)func(std::get<Is>(values), std::get<Is>(streams)), 0)... };
      }

      template<class F, template <typename> class A, template <typename> class B, class...Ts>
      void for_each_stream(std::tuple<A<Ts>...> const & values, std::tuple<B<Ts>...> & streams, F func){
          for_each_stream_impl(values, streams, func, std::make_index_sequence<sizeof...(Ts)>());
      }

    }

//    namespace sfinae_utils {

//      inline constexpr auto is_container_impl(...) -> std::false_type {
//        return std::false_type{};
//      }

//      template <typename C>
//      constexpr auto is_container_impl(C const* c) ->
//          decltype(begin(*c), end(*c), std::true_type{})
//      {
//        (void)c; // removes warning
//        return std::true_type{};
//      }

//      template <typename C>
//      constexpr auto is_container(C const& c) -> decltype(is_container_impl(&c)) {
//          return is_container_impl(&c);
//      }

//    }

    template <typename T, typename P>
    static inline void writeType(ByteVector & data, T const & value, P & offset)
    {
      std::memcpy(&data.data()[offset], &value, sizeof(T));
//      std::copy(&value, std::next(&value, sizeof(T)), std::next(data.begin(), offset));
      offset += sizeof(T);
    }

    template <typename T, typename P>
    static inline void writeContainer(ByteVector & data, T const & value, P & offset)
    {
      std::copy(value.begin(), value.end(), std::next(data.begin(), offset));
      offset += value.size() *  sizeof(typename T::value_type);
    }

//    template <typename T, typename P>
//    static inline void writeType(ByteVector & data, T const & value, P & offset)
//    {
//      writeTypeImpl<T, P>(data, value, offset, sfinae_utils::is_container(value));
//    }

  }

  struct StreamInfo {
    uint32_t id = 0;
    uint16_t frameLength = 0;
  };
}
