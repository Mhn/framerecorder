#pragma once

#include "FrameRecorder.hpp"

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

namespace framerecorder { namespace reader {

  namespace detail {

    template <typename T>
    struct Stream {
      uint32_t offset = 0;
      uint16_t length = 0;
    };

    template <typename... Ts>
    struct Entry {
      std::tuple<Stream<Ts>...> streams;
    };

    template <typename... Ts>
    struct Record {
      std::vector<Entry<Ts...>> entries;
      uint32_t numFrames = 0;
    };

    template <typename T>
    struct StreamView
    {
      uint32_t position = 0;
      uint16_t repeats = 0;
      uint16_t remaining = 0;
      const Stream<T> * stream;
    };

    template <typename... Ts>
    struct EntryView
    {
      std::tuple<StreamView<Ts>...> streamViews;
    };

    template <typename... Ts>
    struct RecordView
    {
      uint16_t recordIndex = 0;
      uint32_t entryFrame = 0;
      std::vector<EntryView<Ts...>> entryViews;
    };

    static bool readAllBytes(std::string const & filename, framerecorder::detail::ByteVector & data, std::stringstream & log)
    {
      std::ifstream file(filename, std::ios::in | std::ifstream::binary);
      if (!file.is_open())
        log << "Could not open file";
      else
        std::copy(std::istreambuf_iterator<framerecorder::detail::Byte>(file), std::istreambuf_iterator<framerecorder::detail::Byte>(), std::back_inserter(data));

      return file.is_open();
    }

    static inline std::string readString(const framerecorder::detail::ByteVector & data, uint32_t & offset, uint32_t length)
    {
      uint32_t start(offset);
      offset += length;
      return std::string(&data[start], &data[offset]);
    }

    template <typename T>
    static inline T readType(const framerecorder::detail::ByteVector & data, uint32_t & offset)
    {
      T result;
      std::memcpy(&result, data.data() + offset, sizeof(T));
      offset += sizeof(T);
      return result;
    }

    template <typename T>
    static inline void reset(const framerecorder::detail::ByteVector & data, StreamView<T> & streamView, Stream<T> const & stream)
    {
      streamView.stream = &stream;
      streamView.position = stream.offset;
      streamView.remaining = readType<uint16_t>(data, streamView.position);
      uint32_t repeatHdrPos = streamView.position + (streamView.remaining * sizeof(T));
      streamView.repeats = readType<uint16_t>(data, repeatHdrPos);
    }

    template <typename T>
    static inline bool stepFrames(const framerecorder::detail::ByteVector & data, StreamView<T> & streamView, uint16_t frameNumber)
    {
      do
      {

        if (frameNumber > 0)
        {
          if (streamView.remaining > 1)
          {
            streamView.position += sizeof(T);
            frameNumber--;
            streamView.remaining--;
          }
          else if (streamView.repeats > 0)
          {
            frameNumber--;
            streamView.repeats--;
          }
        }
        while (streamView.remaining < 1 || streamView.repeats < 1)
        {
          uint32_t projectedPos = streamView.position + sizeof(T) * streamView.remaining + sizeof(uint16_t);
          if (projectedPos >= streamView.stream->offset + streamView.stream->length)
            return false;

          streamView.position += sizeof(T) * streamView.remaining; // Step over remaining values
          streamView.position += sizeof(uint16_t); // Step over repeat hdr we read last time (or in reset)

          streamView.remaining = readType<uint16_t>(data, streamView.position);
          uint32_t repeatHdrPos = streamView.position + (streamView.remaining * sizeof(T));
          streamView.repeats = readType<uint16_t>(data, repeatHdrPos);
        }
      }
      while (frameNumber > 0);

      return true;
    }

    template <typename T>
    static inline void readFrame(const framerecorder::detail::ByteVector & data, StreamView<T> const & streamView, T & value)
    {
      std::copy(std::next(data.begin(), streamView.position), std::next(data.begin(), streamView.position + sizeof(T)), reinterpret_cast<framerecorder::detail::Byte*>(&value));
    }
  }

  template <typename... Ts>
  class RecordReader
  {
    public:

      RecordReader()
      {
      }

      bool setFrame(uint32_t frame)
      {
        if (frame < m_numFrames)
        {
          uint32_t f(frame);
          for (uint16_t i = 0; i < m_records.size(); i++)
          {
            if (f < m_records[i].numFrames)
            {
              m_recordView.recordIndex = i;
              m_recordView.entryFrame = 0;

              for (uint16_t e = 0; e < m_recordView.entryViews.size(); e++)
              {
                detail::Entry<Ts...> const & entry = m_records[m_recordView.recordIndex].entries[e];
                detail::EntryView<Ts...> & entryView = m_recordView.entryViews[e];

                framerecorder::detail::tuple_utils::for_each_stream(entry.streams, entryView.streamViews, [this](auto &stream, auto &streamView) {
                  detail::reset(m_data, streamView, stream);
                });
              }

              m_curFrame = frame - f;

              return advanceFrames(f);
            }
            f -= m_records[i].numFrames;
          }
        }

        return false;
      }

      bool advanceFrames(uint32_t frames = 1)
      {
        uint32_t targetFrame = m_curFrame + frames;
        uint32_t targetEntryFrame = m_recordView.entryFrame + frames;

        if (targetEntryFrame >= m_records[m_recordView.recordIndex].numFrames)
          return setFrame(targetFrame);

        bool ok = true;

        for (detail::EntryView<Ts...> & entryView : m_recordView.entryViews)
        {
          framerecorder::detail::tuple_utils::for_each_stream(entryView.streamViews, [this, &frames, &ok](auto &streamView) {
            ok = detail::stepFrames(m_data, streamView, frames) && ok;
          });
        }

        if (!ok)
          std::cerr << "not ok!" << std::endl;


        m_recordView.entryFrame = targetEntryFrame;
        m_curFrame = targetFrame;

        return ok;
      }

      bool getValues(std::vector<std::tuple<Ts...>> & values, std::stringstream & log)
      {
        if (values.size() != m_objects.size())
        {
          log << "Error: Number of values does not equal number of objects.";
          return false;
        }

        for (uint16_t i = 0; i < values.size(); i++)
        {
          detail::EntryView<Ts...> const & entryView = m_recordView.entryViews[i];

          framerecorder::detail::tuple_utils::for_each_stream(values[i], entryView.streamViews, [this](auto & value, auto const & streamView) {
            detail::readFrame(m_data, streamView, value);
          });
        }
        return true;
      }

      bool load(framerecorder::detail::ByteVector const & data, uint32_t & offset, std::stringstream & log)
      {
        m_data = data;

        return load(offset, log);
      }

      bool load(std::string const& filename, std::stringstream & log)
      {
        if (!detail::readAllBytes(filename, m_data, log))
          return false;

        uint32_t offset(0);
        return load(offset, log);
      }

      std::string describe()
      {
        std::stringstream ss;
        ss << "Number of streams: " << sizeof...(Ts)    << std::endl
           << "Number of objects: " << m_objects.size() << std::endl
           << "Version: "           << 1                << std::endl
           << "Framerate: "         << m_frameRate      << std::endl
           << "Number of frames: "  << m_numFrames      << std::endl;

        ss << "Records (" << m_records.size() << "):" << std::endl;

        for (detail::Record<Ts...> & record : m_records)
        {
          ss << " " << record.numFrames << std::endl;
        }
        return ss.str();
      }

      std::vector<std::string> const & getObjects() { return m_objects; }
      uint32_t getNumberOfFrames() { return m_numFrames; }
      uint16_t getFrameRate() { return m_frameRate; }
      uint32_t getCurrentFrame() { return m_curFrame; }

    private:

      bool load(uint32_t & offset, std::stringstream & log)
      {
        if (detail::readString(m_data, offset, framerecorder::detail::MAGIC_STRING.length()) != framerecorder::detail::MAGIC_STRING)
        {
          log << "File Magic string match fail";
          return false;
        }

        uint16_t version = detail::readType<uint16_t>(m_data, offset);

        if (version != 1)
        {
          log << "Unsupported file version: " << version;
          return false;
        }

        m_frameRate = detail::readType<uint16_t>(m_data, offset);
        m_numFrames = detail::readType<uint32_t>(m_data, offset);

        uint16_t numRecords = detail::readType<uint16_t>(m_data, offset);
        uint16_t numStreams = detail::readType<uint16_t>(m_data, offset);
        uint16_t numObjects = detail::readType<uint16_t>(m_data, offset);

        if (numStreams != sizeof...(Ts))
        {
          log << "Mismatching number of streams. Expected: " << sizeof...(Ts) << " Contained in file: " << numStreams;
          return false;
        }

        m_objects.resize(numObjects);

        for (std::string & object : m_objects)
        {
          uint16_t strlen = detail::readType<uint16_t>(m_data, offset);
          object = detail::readString(m_data, offset, strlen);
        }

        m_records.resize(numRecords);

        for (detail::Record<Ts...> & record : m_records)
        {
          record.numFrames = detail::readType<uint32_t>(m_data, offset);
          record.entries.resize(numObjects);

          for (detail::Entry<Ts...> & entry : record.entries)
          {
            framerecorder::detail::tuple_utils::for_each_stream(entry.streams, [this, &offset](auto &stream) {
              stream.length = detail::readType<uint16_t>(m_data, offset);
              stream.offset = offset;
              offset += stream.length;
            });
          }
        }

        m_recordView.entryViews.resize(m_objects.size());

        return setFrame(0);
      }

    private:
      uint32_t m_curFrame = 0;
      uint32_t m_numFrames = 0;
      uint16_t m_frameRate = 0;
      framerecorder::detail::ByteVector m_data;
      std::vector<std::string> m_objects;
      std::vector<detail::Record<Ts...>> m_records;
      detail::RecordView<Ts...> m_recordView;
  };

}}
